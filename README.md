# Optimus-tower
## Overview

Optimus-tower is Command Line Interfae tool for managing Amazon Machine Image(AMI below) at Amazon Web Service(AWS below).

## Installation
```
git clone <this repo>
```

## How to use
### config.json
config.json file contains AWS and default AMI configuration requied.
> NOTE: AWS user requied AmazonEC2FullAccess policy.

```
// Example
{
	"AWS": {
		"accessKeyId": "<AWS access key id>",
		"secretAccessKey": "<AWS secret access key>",
		"region": "<Your primary region>"
	},
	"INSTANCE": {
		"DefaultImageId": "<Default AMI image id>",
		"DefaultInstanceType": "<Default instance type>", // Default is m1.small
		"UserData": [
			"#!/bin/bash",
			"<Write shell script you want to invoke when it startup>",
			"<if you want to use multi-line script thwn you write scripts as STRING array>"
		]
	}
}
```

### Command
```
bin/tower option=value <start|stop>
```

### Options
> NOTE: Every options dominates any configurations.

- count: Number of EC2 instance
- start|stop: Define tower action
- imageid: AMI image id
- instancetype: EC2 instance type
